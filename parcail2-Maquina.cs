﻿using System;

namespace ConsoleApp74
{
    class Program
    {
        
              //EJERCICIO-MAQUINA///
            

             class MainClass
             {
            public class Producto
            {
                public int precio = 0;
                public int jugo = 0;
                public int agua = 0;
                public int iceti = 0;
                public int barrita = 0;
                public int melocoton = 0;
                public int pera = 0;
                public int manzana = 0;
                public int avena = 0;
                public int malta = 0;
                public int cafe = 0;

                public void Mostrar()
                {
                    int op;
                    do
                    {
                        Console.WriteLine(" ");
                        Console.WriteLine("Maquina - Health");
                        Console.WriteLine("1. Ingresar moneda");
                        Console.WriteLine("2. Elije un producto");
                        Console.WriteLine("3. Informe Productos vendidos");
                        Console.WriteLine("4. Salir");
                        Console.Write("Ingrese una opcion:");
                        op = int.Parse(Console.ReadLine());
                        Console.WriteLine(" ");
                        if (op == 1)
                        {
                            Console.WriteLine("1. Introduce monedas de $5.");
                            Console.WriteLine("2. Introduce monedas de $10.");
                            Console.WriteLine("3. Introduce monedas de $25.");
                            Console.WriteLine("4. Introduce billetes de $50.");
                            Console.WriteLine("5. Introduce billetes de $100.");
                            Console.WriteLine("6. Introduce billetes de $200.");
                            Console.Write("Elija una opcion: ");
                            int op2 = int.Parse(Console.ReadLine());
                            if (op2 == 1)
                            {
                                Console.Write("Introduce cantidad de moneda: ");
                                precio = int.Parse(Console.ReadLine());

                                precio *= 5;
                                Console.WriteLine("Total de monedas introducidas de 5: " + precio);

                            }
                            if (op2 == 2)
                            {
                                Console.Write("Introduce cantidad de moneda: ");
                                precio = int.Parse(Console.ReadLine());

                                precio *= 10;
                                Console.WriteLine("Total de monedas introducidas de 10: " + precio);

                            }
                            if (op2 == 3)
                            {
                                Console.Write("Introduce cantidad de moneda: ");
                                precio = int.Parse(Console.ReadLine());

                                precio *= 25;
                                Console.Write("Total de monedas introducidas de 25: " + precio);

                            }
                            if (op2 == 4)
                            {
                                Console.WriteLine("Introduce cantidad de billetes: ");
                                precio = int.Parse(Console.ReadLine());

                                precio *= 50;
                                Console.Write("Total de billetes introducidos de 50: " + precio);
                            }
                            if (op2 == 5)
                            {
                                Console.Write("Introduce cantidad de billetes: ");
                                precio = int.Parse(Console.ReadLine());

                                precio *= 100;
                                Console.WriteLine("Total de billetes introducidos de 100: " + precio);
                            }
                            if (op2 == 6)
                            {
                                Console.Write("Introduce cantidad de billetes: ");
                                precio = int.Parse(Console.ReadLine());

                                precio *= 200;
                                Console.WriteLine("Total de billetes introducidos de 200: " + precio);
                            }
                            if (op2 <= 0 || op2 >= 7)
                            {
                                  
                                
                                Console.WriteLine("Billetes o monedas no permitido, intentelo nuevamente");
                            }




                        }
                        if (op == 2)
                        {
                            Console.WriteLine("Productos");
                            Console.WriteLine("1. Jugo Naranja RD$25");
                            Console.WriteLine("2. Agua Planeta RD$10");
                            Console.WriteLine("3. Ice Te RD$25");
                            Console.WriteLine("4. Barra Nutritiva RD$15");
                            Console.WriteLine("5. Jugo Melocoton RD$15");
                            Console.WriteLine("6. Jugo Pera RD$15");
                            Console.WriteLine("7. Jugo Manzana RD$15");
                            Console.WriteLine("8. Avena RD$25");
                            Console.WriteLine("9. Malta Morena 16onz RD$25");
                            Console.WriteLine("10. Cafe RD$10");
                            Console.Write("Elije un producto: ");
                            int m = int.Parse(Console.ReadLine());
                            if (m == 1)


                            {
                                if (precio >= 25)
                                {
                                    Console.WriteLine("Has Comprado un Jugo de Naranja");
                                    precio -= 25;
                                    jugo += 1;
                                    Console.WriteLine("Te quedan RD$ {0}", precio);
                                }
                                else
                                {
                                    Console.WriteLine("Dinero insuficiente");
                                }
                            }
                            if (m == 2)
                            {
                                if (precio >= 25)
                                {
                                    Console.WriteLine("Has Comprado una Agua Planeta");
                                    precio -= 10;
                                    agua += 1;
                                    Console.WriteLine("Te quedan RD$ {0}", precio);
                                }
                                else
                                {
                                    Console.WriteLine("Dinero insuficiente");
                                }
                            }
                            if (m == 3)
                            {
                                if (precio >= 25)
                                {
                                    Console.WriteLine("Has Comprado un Ice Te");
                                    precio -= 25;
                                    iceti += 1;
                                    Console.WriteLine("Te quedan RD$ {0}", precio);
                                }
                                else
                                {
                                    Console.WriteLine("Dinero insuficiente");
                                }
                            }
                            if (m == 4)
                            {
                                if (precio >= 10)
                                {
                                    Console.WriteLine("Has Comprado una Barrita ");
                                    precio -= 15;
                                    barrita += 1;
                                    Console.WriteLine("Te quedan RD$ {0}", precio);
                                }
                                else
                                {
                                    Console.WriteLine("Dinero insuficiente");
                                }
                            }
                            if (m == 5)
                            {
                                if (precio >= 15)
                                {
                                    Console.WriteLine("Has Comprado un Jugo de Melocoton");
                                    precio -= 15;
                                    melocoton += 1;
                                    Console.WriteLine("Te quedan RD$ {0}", precio);
                                }
                                else
                                {
                                    Console.WriteLine("Dinero insuficiente");
                                }

                            }
                            if (m == 6)
                            {
                                if (precio >= 15)
                                {
                                    Console.WriteLine("Has Comprado un Jugo de Pera");
                                    precio -= 15;
                                    pera += 1;
                                    Console.WriteLine("Te quedan RD$ {0}", precio);
                                }
                                else
                                {
                                    Console.WriteLine("Dinero insuficiente");
                                }

                            }
                            if (m == 7)
                            {
                                if (precio >= 15)
                                {
                                    Console.WriteLine("Has Comprado un Jugo de Manzana");
                                    precio -= 15;
                                    manzana += 1;
                                    Console.WriteLine("Te quedan RD$ {0}", precio);
                                }
                                else
                                {
                                    Console.WriteLine("Dinero insuficiente");
                                }

                            }
                            if (m == 8)
                            {
                                if (precio >= 25)
                                {
                                    Console.WriteLine("Has Comprado un Avena Instantanea");
                                    precio -= 25;
                                    avena += 1;
                                    Console.WriteLine("Te quedan RD$ {0}", precio);
                                }
                                else
                                {
                                    Console.WriteLine("Dinero insuficiente");
                                }

                            }
                            if (m == 9)
                            {
                                if (precio >= 25)
                                {
                                    Console.WriteLine("Has Comprado una Malta Morena 16onz");
                                    precio -= 25;
                                    malta += 1;
                                    Console.WriteLine("Te quedan RD$ {0}", precio);
                                }
                                else
                                {
                                    Console.WriteLine("Dinero insuficiente");
                                }

                            }
                            if (m == 10)
                            {
                                if (precio >= 10)
                                {
                                    Console.WriteLine("Has Comprado un Cafe");
                                    precio -= 10;
                                    cafe += 1;
                                    Console.WriteLine("Te quedan RD$ {0}", precio);
                                }
                                else
                                {
                                    Console.WriteLine("Dinero insuficiente");
                                }
                            }
                            if (m >= 11 || m <= 0)
                            {
                                Console.WriteLine("Invalido");
                            }
                        }
                        //

                    } while (op != 3);
                }
                public void info()
                {
                    Console.WriteLine("Productos");
                    Console.WriteLine("1. Jugo Naranja RD$25");
                    Console.WriteLine("2.  RD$10");
                    Console.WriteLine("3.  RD$25");
                    Console.WriteLine("4.  RD$15");
                    Console.WriteLine("5.  RD$15");
                    Console.WriteLine("6. Jugo Pera RD$15");
                    Console.WriteLine("7. Jugo Manzana RD$15");
                    Console.WriteLine("8. Avena RD$25");
                    Console.WriteLine("9. Malta Morena 16onz RD$25");
                    Console.WriteLine("10. Cafe RD$10");
                    Console.Write("Elije un producto: ");


                    Console.WriteLine("*Productos Vendidos*");
                    Console.WriteLine("1- Jugo Naranja vendidas: " + jugo);
                    Console.WriteLine("2- Agua Planeta vendidas: " + agua);
                    Console.WriteLine("3- Ice Te vendidas: " + iceti);
                    Console.WriteLine("4- Barra Nutritiva vendidas: " + barrita);
                    Console.WriteLine("5- Jugo de Melocoton vendidos: " + melocoton);
                    Console.WriteLine("6- Jugo de Pera vendidos: " + pera);
                    Console.WriteLine("7- Jugo de Manzanas vendidos: " + manzana);
                    Console.WriteLine("8- Avena Instantanea vendidos: " + avena);
                    Console.WriteLine("9- Malta morena 16onz vendidos: " + malta);
                    Console.WriteLine("10- Cafe vendidos: " + cafe);
                    Console.ReadKey();
                }
            }

            public static void Main(string[] args)
            {
                Producto pr = new Producto();
                pr.Mostrar();
                pr.info();
            }
             }
    }
    
}
    
   
